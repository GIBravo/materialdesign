module latte {

    /**
     *
     */
    export class MCardMedia extends MCardMediaBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods
        //endregion

        //region Events
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _height: number = null;

        /**
         * Gets or sets height
         *
         * @returns {number}
         */
        get height(): number {
            this._height = parseFloat(this.style.height);
            return this._height;
        }

        /**
         * Gets or sets height
         *
         * @param {number} value
         */
        set height(value: number) {
            this._height = value;
            this.style.height = value +"px";
        }
        //endregion

    }

}