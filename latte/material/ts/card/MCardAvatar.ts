module latte {

    /**
     *
     */
    export class MCardAvatar extends MCardAvatarBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods
        //endregion

        //region Events
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _title: string = "";

        /**
         * Gets or sets title
         *
         * @returns {string}
         */
        get title(): string {
            return this._title;
        }

        /**
         * Gets or sets title
         *
         * @param {string} value
         */
        set title(value: string) {
            this.stringTitle.text = value;
            this._title = value;
        }

        /**
         * Property field
         */
        private _subtitle: string = "";

        /**
         * Gets or sets subtitle
         *
         * @returns {string}
         */
        get subtitle(): string {
            return this._subtitle;
        }

        /**
         * Gets or sets subtitle
         *
         * @param {string} value
         */
        set subtitle(value: string) {
            this.stringSubtitle.style.display = "initial";
            this.stringSubtitle.text = value;
            this._subtitle = value;
        }
        //endregion

    }

}