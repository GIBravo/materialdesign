module latte {

    /**
     *
     */
    export class MCardVertical extends MCardVerticalBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods
        setSizeMedia(size: string){
            let element = this.querySelector(".media");
            element.classList.add(size);
        }
        //endregion

        //region Events
        //endregion

        //region Properties
        //endregion

    }

}