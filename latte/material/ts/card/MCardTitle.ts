module latte {

    /**
     *
     */
    export class MCardTitle extends MCardTitleBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor( Title: string, Subtitle?: string) {
            super();
            this.title = Title;
            if(Subtitle) this.subtitle = Subtitle;
        }

        //region Private Methods
        //endregion

        //region Methods
        //endregion

        //region Events
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _title: string = "";

        /**
         * Gets or sets title
         *
         * @returns {string}
         */
        get title(): string {
            return this._title;
        }

        /**
         * Gets or sets title
         *
         * @param {string} value
         */
        set title(value: string) {
            this._title = value;
            this.stringTitle.text = value;
        }

        /**
         * Property field
         */
        private _subtitle: string = "";

        /**
         * Gets or sets subtitle
         *
         * @returns {string}
         */
        get subtitle(): string {
            return this._subtitle;
        }

        /**
         * Gets or sets subtitle
         *
         * @param {string} value
         */
        set subtitle(value: string) {
            this._subtitle = value;
            this.stringSubtitle.style.display = "initial";
            this.stringSubtitle.text = value;
        }
        //endregion

    }

}