module latte {

    /**
     *
     */
    export class MCard extends MCardBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();

            this.elevation = 1;
        }

        //region Private Methods
        //endregion

        //region Methods
        //endregion

        //region Events
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _elevation: number = 0;

        /**
         * Gets or sets elevation
         *
         * @returns {number}
         */
        get elevation(): number {
            return this._elevation;
        }

        /**
         * Gets or sets elevation
         *
         * @param {number} value
         */
        set elevation(value: number) {
            this._elevation = value;
            this.style.boxShadow = 'var(--shadow-'+value+')';
        }


        /**
         * Property field
         */
        private _width: number = null;

        /**
         * Gets or sets width
         *
         * @returns {number}
         */
        get width(): number {
            this._width = parseFloat(this.style.width);
            return this._width;
        }

        /**
         * Gets or sets width
         *
         * @param {number} value
         */
        set width(value: number) {
            this._width = value;
            this.style.width = value +"px";
        }
        //endregion

    }

}