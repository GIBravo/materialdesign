module latte {

    // Material Interface Metadata
    export interface MIMetadata{
        charset?: string;
        content?: string;
        httpEquiv?: string;
        name?: string;
        scheme?: string;
    }

}