module latte {

    /**
     *
     */
    export class MElement extends Element<HTMLElement> {

        /**
         *
         */
        constructor(tag: string = 'div') {
            super(document.createElement(tag));
        }

    }

}