module latte {

    /**
     *
     */
    export class MMetadata extends Array {

        //region Static

        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods
        add(meta: MIMetadata) {
            this.push(meta);

            let tag = document.createElement('meta');
            if(meta.charset) tag.charset = meta.charset;
            if(meta.content) tag.content = meta.content;
            if(meta.httpEquiv) tag.httpEquiv = meta.httpEquiv;
            if(meta.name) tag.name = meta.name;
            if(meta.scheme) tag.scheme = meta.scheme;
            document.head.appendChild(tag);
        }
        //endregion

        //region Events
        //endregion

        //region Properties
        //endregion

    }

}