module latte {

    /**
     *
     */
    export class MMenu extends MMenuBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();

            this.onLoad();
        }

        //region Private Methods
        private onLoad(){
            this.stringIcon.addEventListener('click',() => this.showMenu());

            window.addEventListener('click', ev => {

                if( ev.target == this.stringIcon.raw)
                    this.showMenu();
                else
                    this.hideMenu();

            });
        }
        //endregion

        //region Methods
        showMenu(){
            let items = this.items.getCollection();
            let size = 0;
            let dividers = 0;
            for(let i=0; i<items.length; i++)
                if(items[i] instanceof  MMenuItem) size++;
                else if(items[i] instanceof MHDivider) dividers+=2;

            this.items.style.height = 36 * size + dividers + "px";
            this.items.addClass('show');
        }

        hideMenu(){
            this.items.style.height = 0 + "px";
            this.items.removeClass('show');
        }
        //endregion

        //region Events
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _color: string = null;

        /**
         * Gets or sets icon color
         *
         * @returns {string}
         */
        get color(): string {
            return this._color;
        }

        /**
         * Gets or sets icon color
         *
         * @param {string} value
         */
        set color(value: string) {
            this._color = value;
            this.stringIcon.addClass(value);
        }

        /**
         * Property field
         */
        private _icon: string = "";

        /**
         * Gets or sets icon
         *
         * @returns {string}
         */
        get icon(): string {
            return this._icon;
        }

        /**
         * Gets or sets icon
         *
         * @param {string} value
         */
        set icon(value: string) {
            this._icon = value;
            this.stringIcon.text = value;
            this.stringIcon.style.display = "flex";
        }

        /**
         * Property field
         */
        private _size: number = null;

        /**
         * Gets or sets icon size
         *
         * @returns {number}
         */
        get size(): number {
            return this._size;
        }

        /**
         * Gets or sets icon size
         *
         * @param {number} value
         */
        set size(value: number) {
            this._size = value;

            this.style.width = value + "px";
            this.style.height = value + "px";
            this.stringIcon.style.fontSize = value + "px";
            this.stringIcon.style.width = value + "px";
            this.stringIcon.style.height = value + "px";
        }
        //endregion

    }

}