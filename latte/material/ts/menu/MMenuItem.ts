module latte {

    /**
     *
     */
    export class MMenuItem extends MMenuItemBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.addEventListener('click',() => this.onClick());
        }

        //region Private Methods
        //endregion

        //region Methods

        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when click item
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _text: string = "";

        /**
         * Gets or sets text
         *
         * @returns {string}
         */
        get text(): string {
            return this._text;
        }

        /**
         * Gets or sets text
         *
         * @param {string} value
         */
        set text(value: string) {
            this._text = value;
            this.stringItem.text = value;
        }

        /**
         * Property field
         */
        private _icon: string = "";

        /**
         * Gets or sets icon
         *
         * @returns {string}
         */
        get icon(): string {
            return this._icon;
        }

        /**
         * Gets or sets icon
         *
         * @param {string} value
         */
        set icon(value: string) {
            this._icon = value;
            this.stringIcon.text = value;
            this.stringIcon.style.display = "flex";
        }
        //endregion

    }

}