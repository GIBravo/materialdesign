module latte {

    /**
     *
     */
    export class MProgressIndeterminate extends MProgressIndeterminateBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.hide();
        }

        //region Private Methods
        //endregion

        //region Methods
        show(){
            this.style.display = 'flex';
            console.log(this.style.display);
        }

        hide(){
            this.style.display = 'none';
        }
        //endregion

        //region Events
        //endregion

        //region Properties
        //endregion

    }

}