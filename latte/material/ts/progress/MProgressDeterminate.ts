module latte {

    /**
     *
     */
    export class MProgressDeterminate extends MProgressDeterminateBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods
        //endregion

        //region Events
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _value: number = 0;

        /**
         * Gets or sets progress value
         *
         * @returns {number}
         */
        get value(): number {
            return this._value;
        }

        /**
         * Gets or sets progress value
         *
         * @param {number} value
         */
        set value(value: number) {
            this._value = value;
            this.progressValue.style.width = value + "%";
        }
        //endregion

    }

}