module latte {

    /**
     *
     */
    export class MButtonIcon extends MButtonIconBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor(icon: string, color?: string) {
            super();
            this.icon = icon;
            if(color) this.color = color;

            this.addEventListener('click',() => this.onClick());
        }

        //region Private Methods
        //endregion

        //region Methods
        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when the button is pressed
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _color: string = null;

        /**
         * Gets or sets color
         *
         * @returns {string}
         */
        get color(): string {
            return this._color;
        }

        /**
         * Gets or sets color
         *
         * @param {string} value
         */
        set color(value: string) {
            this._color = value;
            this.addClass(value);
        }

        /**
         * Property field
         */
        private _icon: string = null;

        /**
         * Gets or sets icon
         *
         * @returns {string}
         */
        get icon(): string {
            return this._icon;
        }

        /**
         * Gets or sets icon
         *
         * @param {string} value
         */
        set icon(value: string) {
            this._icon = value;
            this.text = value;
        }

        /**
         * Property field
         */
        private _size: number = null;

        /**
         * Gets or sets icon size
         *
         * @returns {number}
         */
        get size(): number {
            return this._size;
        }

        /**
         * Gets or sets icon size
         *
         * @param {number} value
         */
        set size(value: number) {
            this._size = value;
            this.style.width = value + "px";
            this.style.height = value + "px";

            let porcent = value * 80 / 100;
            this.style.fontSize = porcent + "px";
        }
        //endregion

    }

}