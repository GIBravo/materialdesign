module latte {

    /**
     *
     */
    export class MButton extends MButtonBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor(text?: string, icon?: string, color?: string, bcolor?: string) {
            super();

            if (icon) this.setIcon(icon, color);
            if (text) this.setText(text, color);
            if (color) this.setColor(color);
            if (bcolor) this.setBackgroundColor(bcolor);

            this.addEventListener('click',() => this.onClick());
        }


        //region Private Methods
        //endregion

        //region Methods
        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }

        setColor(color: string){
            this.raw.classList.add(color);
        }

        setBackgroundColor(color: string){
            this.raw.classList.add("color");
            this.raw.classList.add("b"+color);
        }

        setIcon(icon: string, color?: string){
            let itemIcon;

            if(color)
                itemIcon = new MIcon(icon, color);
            else
                itemIcon = new MIcon(icon);

            this.add(itemIcon);
        }

        setText(text: string, color?: string){
            let itemText;

            if(color)
                itemText = new MText(text, color);
            else
                itemText = new MText(text);

            this.add(itemText);
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when the button is pressed
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }
        //endregion

        //region Properties
        //endregion

    }

}