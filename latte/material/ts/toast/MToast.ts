module latte {

    /**
     *
     */
    export class MToast extends MToastBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.onLoad();
        }

        //region Private Methods
        private onLoad(){
            this.buttonAction.addEventListener('click',() => this.onClick());
        }
        //endregion

        //region Methods
        show(message: string, time?: number){
            this.stringMessage.text = message;
            this.raw.className = 'show';
            window.setTimeout(() => {
                this.raw.className = '';
            }, time || 3000);
        }

        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when action click
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }

        /**
         * Property field
         */
        private _color: string = "yellow";

        /**
         * Gets or sets action color
         *
         * @returns {string}
         */
        get color(): string {
            return this._color;
        }

        /**
         * Gets or sets action color
         *
         * @param {string} value
         */
        set color(value: string) {
            this._color = value;
            this.buttonAction.style.color = "var(--'"+value+"')";
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _action: string = "";

        /**
         * Gets or sets action text
         *
         * @returns {string}
         */
        get action(): string {
            return this._action;
        }

        /**
         * Gets or sets action text
         *
         * @param {string} value
         */
        set action(value: string) {
            this._action = value;
            this.buttonAction.style.display = "initial";
            this.buttonAction.text = value;
        }
        //endregion

    }

}