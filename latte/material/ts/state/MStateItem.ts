module latte {

    /**
     *
     */
    export class MStateItem extends MStateItemBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor(icon: string, value: number) {
            super();
            this.icon = icon;
            this.value = value;
            this.addEventListener('click',() => this.onClick());
        }

        //region Private Methods
        //endregion

        //region Methods
        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when icon click
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _active: boolean = false;

        /**
         * Gets or sets item state
         *
         * @returns {boolean}
         */
        get active(): boolean {
            return this._active;
        }

        /**
         * Gets or sets item state
         *
         * @param {boolean} value
         */
        set active(value: boolean) {
            this._active = value;

            if(value) this.addClass('active');
            else   this.removeClass('active');
        }

        /**
         * Property field
         */
        private _icon: string = null;

        /**
         * Gets or sets icon
         *
         * @returns {string}
         */
        get icon(): string {
            return this._icon;
        }

        /**
         * Gets or sets icon
         *
         * @param {string} value
         */
        set icon(value: string) {
            this._icon = value;
            this.text = value;
        }

        /**
         * Property field
         */
        private _value: number = 0;

        /**
         * Gets or sets value assign
         *
         * @returns {number}
         */
        get value(): number {
            return this._value;
        }

        /**
         * Gets or sets value assign
         *
         * @param {number} value
         */
        set value(value: number) {
            this._value = value;
        }
        //endregion

    }

}