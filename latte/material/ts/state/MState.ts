module latte {

    /**
     *
     */
    export class MState extends MStateBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods

        /**
         * Raises the <c>state</c> event
         */
        onStateChanged(){
            if(this._stateChanged){
                this._stateChanged.raise();
            }
        }
        //endregion

        //region Events
        /**
         * Back field for event
         */
        private _stateChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the state property changes
         *
         * @returns {LatteEvent}
         */
        get stateChanged(): LatteEvent{
            if(!this._stateChanged){
                this._stateChanged = new LatteEvent(this);
            }
            return this._stateChanged;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _icons: string[] = [];

        /**
         * Gets or sets icons
         *
         * @returns {string[]}
         */
        get icons(): string[] {
            return this._icons;
        }

        /**
         * Gets or sets icons
         *
         * @param {string[]} value
         */
        set icons(value: string[]) {
            this._icons = value;

            this.items.clear();
            value.forEach((icon,index) => {
                let stateItem = new MStateItem(icon, index);
                stateItem.click.add(() => { this.state = stateItem.value; });
                this.items.add(stateItem);
            });

            this.state = 0;
        }


        /**
         * Property field
         */
        private _state: number = null;

        /**
         * Gets or sets change state value
         *
         * @returns {number}
         */
        get state(): number{
            return this._state;
        }

        /**
         * Gets or sets change state value
         *
         * @param {number} value
         */
        set state(value: number){

            // Check if value changed
            let changed: boolean = value !== this._state;

            // Set value
            this._state = value;

            let items = this.items.getCollection();

            for(let i=0; i<items.length; i++){
                if( i == value ) items[i].active = true;
                else             items[i].active = false;
            }


            // Trigger changed event
            if(changed){
                this.onStateChanged();
            }
        }
        //endregion

    }

}