module latte {

    /**
     *
     */
    export class MInput extends MInputBase {

        //region Static
        //endregion

        //region Fields
        private stringTemporal: string = '';
        //endregion

        /**
         *
         */
        constructor(name: string) {
            super();

            this.stringLabel.raw.setAttribute('for', name);
            this.input.raw.setAttribute('id', name);
            this.onLoad();
        }

        //region Private Methods
        private onLoad(){
            this.input.addEventListener('focus', () => {
                this.raw.classList.add('active');
                this.raw.classList.add('label');

                if(this.raw.classList.contains('error'))
                    this.removeError();
            });
            this.input.addEventListener('focusout', () => {
                if (this.input.value == ''){
                    this.raw.classList.remove('label');

                    if(this.required)
                        this.showError('Campo requerido.');
                }else if(!this.regexp.exec(this.input.text))
                    this.showError(this.errorMessage);

                this.raw.classList.remove('active');
            });
            this.stringAction.addEventListener('click',() => this.onClickAction());
            this.input.addEventListener('keydown',ev => {
                if(ev.keyCode == 13) {
                    if(this.regexp && this.regexp.exec(this.value)) {
                        this.removeError();
                        this.onEnter();
                    }
                    else this.showError(this.errorMessage);
                }
            });
        }
        //endregion

        //region Methods
        showError(error: string){
            if(this.helper != '')
                this.stringTemporal = this.helper;

            this.helper = error;

            this.raw.classList.remove('reset');
            this.raw.classList.add('error');
        }

        removeError(){
            console.log(this.stringTemporal);
            if(this.stringTemporal != '')
                this.helper = this.stringTemporal;
            else
                this.stringHelper.style.display = 'none';

            this.raw.classList.remove('error');
            this.raw.classList.add('reset');
        }

        /**
         * Raises the <c>clickAction</c> event
         */
        onClickAction(){
            if(this._clickAction){
                this._clickAction.raise();
            }
            this.input.raw.focus();
        }

        /**
         * Raises the <c>enter</c> event
         */
        onEnter(){
            if(this._enter){
                this._enter.raise();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _clickAction: LatteEvent;

        /**
         * Gets an event raised when click button action
         *
         * @returns {LatteEvent}
         */
        get clickAction(): LatteEvent{
            if(!this._clickAction){
                this._clickAction = new LatteEvent(this);
            }
            return this._clickAction;
        }


        /**
         * Back field for event
         */
        private _enter: LatteEvent;

        /**
         * Gets an event raised when press enter key (13)
         *
         * @returns {LatteEvent}
         */
        get enter(): LatteEvent{
            if(!this._enter){
                this._enter = new LatteEvent(this);
            }
            return this._enter;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _errorMessage: string = '';

        /**
         * Gets or sets error message if the regexp failed
         *
         * @returns {string}
         */
        get errorMessage(): string {
            return this._errorMessage;
        }

        /**
         * Gets or sets error message if the regexp failed
         *
         * @param {string} value
         */
        set errorMessage(value: string) {
            this._errorMessage = value;
        }

        /**
         * Property field
         */
        private _helper: string = "";

        /**
         * Gets or sets help text
         *
         * @returns {string}
         */
        get helper(): string {
            return this._helper;
        }

        /**
         * Gets or sets help text
         *
         * @param {string} value
         */
        set helper(value: string) {
            this._helper = value;
            this.stringHelper.text = value;
            this.stringHelper.style.display = "initial";
            this.style.paddingBottom = "16px";
        }

        /**
         * Property field
         */
        private _icon: string = "";

        /**
         * Gets or sets icon on the left
         *
         * @returns {string}
         */
        get icon(): string {
            return this._icon;
        }

        /**
         * Gets or sets icon on the left
         *
         * @param {string} value
         */
        set icon(value: string) {
            this._icon = value;
            this.iconMain.text = value;
            this.iconMain.style.display = "initial";
        }

        /**
         * Property field
         */
        private _iconAction: string = "";

        /**
         * Gets or sets icon action
         *
         * @returns {string}
         */
        get iconAction(): string {
            return this._iconAction;
        }

        /**
         * Gets or sets icon action
         *
         * @param {string} value
         */
        set iconAction(value: string) {
            this._iconAction = value;
            this.stringAction.text = value;
            this.stringAction.style.display = 'flex';
            this.input.style.paddingRight = '24px';
        }

        /**
         * Property field
         */
        private _label: string = "";

        /**
         * Gets or sets label text
         *
         * @returns {string}
         */
        get label(): string {
            return this._label;
        }

        /**
         * Gets or sets label text
         *
         * @param {string} value
         */
        set label(value: string) {
            this._label = value;
            this.stringLabel.text = value;
            this.stringLabel.style.display = "initial";
            this.input.style.paddingTop = "8px";
        }

        /**
         * Property field
         */
        private _showPassword: boolean = false;

        /**
         * Gets or sets icon to show the password
         *
         * @returns {boolean}
         */
        get showPassword(): boolean {
            return this._showPassword;
        }

        /**
         * Gets or sets icon to show the password
         *
         * @param {boolean} value
         */
        set showPassword(value: boolean) {
            this._showPassword = value;
            if(value){
                this.stringAction.style.display = 'flex';
                this.stringAction.text = 'visibility';
                this.stringAction.raw.onclick = () => {
                    if(this.type == 'password'){
                        this.type = 'text';
                        this.stringAction.text = 'visibility_off';
                    }else if(this.type == 'text'){
                        this.type = 'password';
                        this.stringAction.text = 'visibility';
                    }
                };
            }
        }

        /**
         * Property field
         */
        private _placeholder: string = "";

        /**
         * Gets or sets input placeholder
         *
         * @returns {string}
         */
        get placeholder(): string {
            return this._placeholder;
        }

        /**
         * Gets or sets input placeholder
         *
         * @param {string} value
         */
        set placeholder(value: string) {
            this._placeholder = value;
            this.input.raw.placeholder = value;
        }

        /**
         * Property field
         */
        private _required: boolean = false;

        /**
         * Gets or sets if the value is mandatory
         *
         * @returns {boolean}
         */
        get required(): boolean {
            return this._required;
        }

        /**
         * Gets or sets if the value is mandatory
         *
         * @param {boolean} value
         */
        set required(value: boolean) {
            this._required = value;
        }
        
        /**
         * Property field
         */
        private _type: string = "";

        /**
         * Gets or sets input type
         *
         * @returns {string}
         */
        get type(): string {
            return this._type;
        }

        /**
         * Gets or sets input type
         *
         * @param {string} value
         */
        set type(value: string) {
            this._type = value;
            this.input.raw.type = value;
        }

        /**
         * Property field
         */
        private _value: string = "";

        /**
         * Gets or sets input value
         *
         * @returns {string}
         */
        get value(): string {
            this._value = this.input.text;
            return this._value;
        }

        /**
         * Gets or sets input value
         *
         * @param {string} value
         */
        set value(value: string) {
            this._value = value;
            this.input.value = value;
        }

        /**
         * Property field
         */
        private _regexp: RegExp = null;

        /**
         * Gets or sets regular expression
         *
         * @returns {RegExp}
         */
        get regexp(): RegExp {
            return this._regexp;
        }

        /**
         * Gets or sets regular expression
         *
         * @param {RegExp} value
         */
        set regexp(value: RegExp) {
            this._regexp = value;
        }
        //endregion

    }

}