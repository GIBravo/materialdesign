module latte {

    /**
     *
     */
    export class MLoader extends MLoaderBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods
        show(){
            this.raw.classList.add('show');
        }

        hide(){
            this.raw.classList.remove('show');
        }
        //endregion

        //region Events
        //endregion

        //region Properties
        //endregion

    }

}