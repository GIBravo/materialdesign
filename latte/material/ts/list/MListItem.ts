module latte {

    /**
     *
     */
    export class MListItem extends MListItemBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor(primary: string) {
            super();
            this.primary = primary;
            this.addEventListener('click',() => this.onClick());
        }

        //region Private Methods
        //endregion

        //region Methods
        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when click
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _avatar: string = null;

        /**
         * Gets or sets avatar icon
         *
         * @returns {string}
         */
        get avatar(): string {
            return this._avatar;
        }

        /**
         * Gets or sets avatar icon
         *
         * @param {string} value
         */
        set avatar(value: string) {
            this._avatar = value;
            this.iconAvatar.text = value;
            this.iconAvatar.style.display = 'flex';
        }

        /**
         * Property field
         */
        private _action: string = null;

        /**
         * Gets or sets action icon
         *
         * @returns {string}
         */
        get action(): string {
            return this._action;
        }

        /**
         * Gets or sets action icon
         *
         * @param {string} value
         */
        set action(value: string) {
            this._action = value;
            this.iconAction.text = value;
            this.iconAction.style.display = 'flex';
        }

        /**
         * Property field
         */
        private _primary: string = null;

        /**
         * Gets or sets primary text
         *
         * @returns {string}
         */
        get primary(): string {
            return this._primary;
        }

        /**
         * Gets or sets primary text
         *
         * @param {string} value
         */
        set primary(value: string) {
            this._primary = value;
            this.stringPrimary.text = value;
        }

        /**
         * Property field
         */
        private _secondary: string = null;

        /**
         * Gets or sets secondary text
         *
         * @returns {string}
         */
        get secondary(): string {
            return this._secondary;
        }

        /**
         * Gets or sets secondary text
         *
         * @param {string} value
         */
        set secondary(value: string) {
            this._secondary = value;
            this.stringSecondary.text = value;
            this.stringSecondary.style.display = 'flex';
        }

        /**
         * Property field
         */
        private _obj: any = null;

        /**
         * Gets or sets any object
         *
         * @returns {any}
         */
        get obj(): any {
            return this._obj;
        }

        /**
         * Gets or sets any object
         *
         * @param {any} value
         */
        set obj(value: any) {
            this._obj = value;
        }
        //endregion

    }

}