module latte {

    /**
     *
     */
    export class MIcon extends MIconBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor(icon: string, color?: string) {
            super();
            this.text = icon;

            if (color) this.setColor(color);
        }

        //region Private Methods
        //endregion

        //region Methods
        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }

        setColor(color: string){
            this.raw.classList.add(color);
        }
        //endregion

        //region Events
        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when the icon is pressed
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }
        //endregion

        //region Properties
        //endregion

    }

}