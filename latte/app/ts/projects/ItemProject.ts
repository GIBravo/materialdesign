module latte {

    /**
     *
     */
    export class ItemProject extends ItemProjectBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.onLoad();
        }

        //region Private Methods
        private archive(){
            this.progress.show();
            this.project.archived = true;
            this.project.save(() => {
                this.project = null;
                this.progress.hide();
            });
        }

        private editName(){
            this.project.name = this.name.text;
            this.project.save();
        }

        private delete(){
            this.progress.show();
            this.project.trash = true;
            this.project.save(() => {
                this.project = null;
                this.progress.hide();
            });
        }

        private share(){
            this.progress.show();
        }

        private onLoad(){
            this.MProgress.add(this.progress);
            this.MMenu.add(this.menu);
            this.icon.text = 'folder';
            this.icon.addEventListener('click',() => this.onClick());

            this.addEventListener('mousemove',() => {
                this.menu.stringIcon.style.opacity = '1';
            });

            this.addEventListener('mouseout', () => {
                this.menu.stringIcon.style.opacity = '0';
            });

            LocalEditor.onClick(this.name.raw,() => this.editName());
        }
        //endregion

        //region Methods
        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }

        /**
         * Raises the <c>project</c> event
         */
        onProjectChanged(){
            if(this._projectChanged){
                this._projectChanged.raise();
            }

            if(this.project != null)
                this.name.text = this.project.name;
        }
        //endregion

        //region Events


        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when click
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }

        /**
         * Back field for event
         */
        private _projectChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the project property changes
         *
         * @returns {LatteEvent}
         */
        get projectChanged(): LatteEvent{
            if(!this._projectChanged){
                this._projectChanged = new LatteEvent(this);
            }
            return this._projectChanged;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _project: Project = null;

        /**
         * Gets or sets project
         *
         * @returns {Project}
         */
        get project(): Project{
            return this._project;
        }

        /**
         * Gets or sets project
         *
         * @param {Project} value
         */
        set project(value: Project){

            // Check if value changed
            let changed: boolean = value !== this._project;

            // Set value
            this._project = value;

            // Trigger changed event
            if(changed){
                this.onProjectChanged();
            }
        }
        //endregion

        //region Components
        /**
         * Field for menu property
         */
        private _menu: MMenu;

        /**
         * Gets menu
         *
         * @returns {MMenu}
         */
        get menu(): MMenu {
            if (!this._menu) {
                this._menu = new MMenu();
                let menu = this._menu;

                menu.stringIcon.style.opacity = '0';

                let menuDelete = new MMenuItem();
                menuDelete.text = strings.delete;
                menuDelete.click.add(() => this.delete());

                let menuArchive = new MMenuItem();
                menuArchive.text = strings.toArchive;
                menuArchive.click.add(() => this.archive());

                let menuShare = new MMenuItem();
                menuShare.text = strings.share;
                menuShare.click.add(() => this.share());

                menu.items.add(menuShare);
                menu.items.add(menuArchive);
                menu.items.add(menuDelete);
            }
            return this._menu;
        }

        /**
         * Field for progress property
         */
        private _progress: MProgressIndeterminate;

        /**
         * Gets progress
         *
         * @returns {MProgressIndeterminate}
         */
        get progress(): MProgressIndeterminate {
            if (!this._progress) {
                this._progress = new MProgressIndeterminate();
            }
            return this._progress;
        }

        //endregion
    }

}