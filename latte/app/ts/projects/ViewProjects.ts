module latte {

    /**
     *
     */
    export class ViewProjects extends ViewProjectsBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();

            this.onLoad();
        }

        //region Private Methods
        private onLoad() {
            this.MNavbar.add(this.navbar);
            this.MButton.add(this.buttonNewProject);
            this.MState.add(this.state);

            this.loadProjects();
        }
        //endregion

        //region Methods
        appendProject(project: Project){
            let card = new MCard();
            let support = new MCardSupporting();
            let itemProject = new ItemProject();

            itemProject.project = project;

            itemProject.click.add(() => { this.projectSelected = project; });
            itemProject.projectChanged.add(() => {
                if (itemProject.project == null) {
                    card.removeFromParent();
                    card = null;
                }
            });

            support.add(itemProject);
            card.add(support);

            support.style.padding = "16px 0";

            card.width = 150;
            card.elevation = 1;

            this.itemsProject.add(card);
        }
        
        loadProjects() {
            Project.search({archived: 'false', trash: 'false'}).send(projects => {
                this.itemsProject.clear();
                if (projects.length > 0)
                    projects.forEach(project => this.appendProject(project));
            });
        }

        /**
         * Raises the <c>createProject</c> event
         */
        onCreateProject() {
            if (this._createProject) {
                this._createProject.raise();
            }
        }

        /**
         * Raises the <c>logout</c> event
         */
        onLogout() {
            if (this._logout) {
                this._logout.raise();
            }
        }

        /**
         * Raises the <c>projectSelected</c> event
         */
        onProjectSelectedChanged() {
            if (this._projectSelectedChanged) {
                this._projectSelectedChanged.raise();
            }
        }
        //endregion

        //region Events
        /**
         * Back field for event
         */
        private _createProject: LatteEvent;

        /**
         * Gets an event raised when create project
         *
         * @returns {LatteEvent}
         */
        get createProject(): LatteEvent {
            if (!this._createProject) {
                this._createProject = new LatteEvent(this);
            }
            return this._createProject;
        }

        /**
         * Back field for event
         */
        private _logout: LatteEvent;

        /**
         * Gets an event raised when user logout
         *
         * @returns {LatteEvent}
         */
        get logout(): LatteEvent {
            if (!this._logout) {
                this._logout = new LatteEvent(this);
            }
            return this._logout;
        }

        /**
         * Back field for event
         */
        private _projectSelectedChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the projectSelected property changes
         *
         * @returns {LatteEvent}
         */
        get projectSelectedChanged(): LatteEvent {
            if (!this._projectSelectedChanged) {
                this._projectSelectedChanged = new LatteEvent(this);
            }
            return this._projectSelectedChanged;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _projectSelected: Project = null;

        /**
         * Gets or sets project selected
         *
         * @returns {Project}
         */
        get projectSelected(): Project {
            return this._projectSelected;
        }

        /**
         * Gets or sets project selected
         *
         * @param {Project} value
         */
        set projectSelected(value: Project) {

            // Check if value changed
            let changed: boolean = value !== this._projectSelected;

            // Set value
            this._projectSelected = value;

            // Trigger changed event
            if (changed) {
                this.onProjectSelectedChanged();
            }
        }
        //endregion

        //region Components
        /**
         * Field for buttonNewProject property
         */
        private _buttonNewProject: MButton;

        /**
         * Gets button new project
         *
         * @returns {MButton}
         */
        get buttonNewProject(): MButton {
            if (!this._buttonNewProject) {
                this._buttonNewProject = new MButton(strings.newProject,'create_new_folder','accent');
                let button = this._buttonNewProject;

                button.click.add(() => {
                    Project.create({
                        name: strings.newProject
                    }).send(project => {
                        this.appendProject(project);
                    });
                });
            }
            return this._buttonNewProject;
        }

        /**
         * Field for navbar property
         */
        private _navbar: MNavbar;

        /**
         * Gets navbar
         *
         * @returns {MNavbar}
         */
        get navbar(): MNavbar {
            if (!this._navbar) {
                this._navbar = new MNavbar();
                let navbar = this._navbar;

                let imageAvatar = <Element<HTMLImageElement>> new MElement('img');
                imageAvatar.element.src = strings.imageGoplekOriginal;

                navbar.itemsLeft.add(imageAvatar);
                navbar.itemsRight.add(this.sessionMenu);
            }
            return this._navbar;
        }

        /**
         * Field for sessionMenu property
         */
        private _sessionMenu: MMenu;

        /**
         * Gets sesion menu
         *
         * @returns {MMenu}
         */
        get sessionMenu(): MMenu {
            if (!this._sessionMenu) {
                this._sessionMenu = new MMenu();
                let menu = this._sessionMenu;

                menu.color = "white";
                menu.icon = "account_circle";

                let closeSession = new MMenuItem();
                closeSession.text = strings.logout;
                closeSession.click.add(() => this.onLogout());

                menu.items.add(closeSession);
            }
            return this._sessionMenu;
        }

        /**
         * Field for state property
         */
        private _state: MState;

        /**
         * Gets state
         *
         * @returns {MState}
         */
        get state(): MState {
            if (!this._state) {
                this._state = new MState();
                let state = this._state;
                state.icons = ['work', 'archive'];
                state.state = 0;
            }
            return this._state;
        }
        //endregion

    }

}