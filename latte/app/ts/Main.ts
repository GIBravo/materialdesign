module latte {

    /**
     * Main Class
     * Your app starts here.
     */
    export class Main {

        /**
         * Start your program on the constructor.
         */
        constructor() {
            this.onLoad();
        }

        //region Private Methods
        private isLogin(){
            this.loader.show();
            User.isLogin().send(response => {
                (response)? this.setView(this.viewProjects) : this.setView(this.viewSession);
                this.loader.hide();
            });
        }

        private loadProjectView(view: ViewProject){

        }

        private loadProjectsView(view: ViewProjects){
            view.logout.add(() => this.logout());

            view.projectSelectedChanged.add(() => {
                this.viewProject.project = view.projectSelected;
                this.setView(this.viewProject);
            });
        }

        private loadSessionView(view: ViewSession){
            view.isLogged.add(() => this.setView(this.viewProjects));
        }

        private onLoad(){
            let meta = new MMetadata();
            meta.add({name: "viewport", content: "width=device-width, initial-scale=1.0"});
            meta.add({httpEquiv:"cache-control", content:"no-cache"});

            let rootview = new View();
            rootview.container.append(this.viewRoot.raw);
            rootview.container.append(this.loader.raw);

            View.mainView = rootview;
            this.isLogin();
        }
        //endregion

        //region Methods


        logout(){
            this.loader.show();
            User.logout().send(response => {
                if(response) this.setView(this.viewSession);
                this.loader.hide();
            });
        }

        setView(view){
            this.viewRoot.items.clear();
            this.viewRoot.items.add(this.loader);
            this.viewRoot.items.add(view);
        }
        //endregion

        //region Events
        //endregion

        //region Properties
        //endregion

        //region components
        /**
         * Field for viewProject property
         */
        private _viewProject: ViewProject;

        /**
         * Gets project view
         *
         * @returns {ViewProject}
         */
        get viewProject(): ViewProject {
            if (!this._viewProject) {
                this._viewProject = new ViewProject();
                this.loadProjectView(this._viewProject);
            }
            return this._viewProject;
        }

        /**
         * Field for viewProjects property
         */
        private _viewProjects: ViewProjects;

        /**
         * Gets projects view
         *
         * @returns {ViewProjects}
         */
        get viewProjects(): ViewProjects {
            if (!this._viewProjects) {
                this._viewProjects = new ViewProjects();
                this.loadProjectsView(this.viewProjects);
            }
            return this._viewProjects;
        }

        /**
         * Field for viewRoot property
         */
        private _viewRoot: ViewRoot;

        /**
         * Gets root view
         *
         * @returns {ViewRoot}
         */
        get viewRoot(): ViewRoot {
            if (!this._viewRoot) {
                this._viewRoot = new ViewRoot();
            }
            return this._viewRoot;
        }

        /**
         * Field for viewSession property
         */
        private _viewSession: ViewSession;

        /**
         * Gets session view
         *
         * @returns {ViewSession}
         */
        get viewSession(): ViewSession {
            if (!this._viewSession) {
                this._viewSession = new ViewSession();
                this.loadSessionView(this._viewSession);
            }
            return this._viewSession;
        }

        /**
         * Field for loader property
         */
        private _loader: MLoader;

        /**
         * Gets loader
         *
         * @returns {MLoader}
         */
        get loader(): MLoader {
            if (!this._loader) {
                this._loader = new MLoader();
            }
            return this._loader;
        }
        //endregion

    }

}