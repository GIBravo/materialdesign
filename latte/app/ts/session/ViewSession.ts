module latte {

    /**
     *
     */
    export class ViewSession extends ViewSessionBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.onLoad();
        }

        //region Private Methods
        private onLoad(){
            this.add(this.toast);
            this.MLogin.add(this.cardLogin);
        }

        private login(username: string, password: string){
            this.progress.show();

            let options = {};
            options['name'] = username;
            options['password'] = password;

            User.login(options).send(users => {
                this.progress.hide();
                if(users){
                    this.inputUser.value = "";
                    this.inputPassword.value = "";
                    this.onIsLogged();
                }else {
                    this.toast.show(strings.errorSession, 3000);
                    this.inputUser.showError(strings.errorUsername);
                    this.inputPassword.showError(strings.errorPassword);
                }
            });

        }
        //endregion

        //region Methods

        /**
         * Raises the <c>isLogged</c> event
         */
        onIsLogged(){
            if(this._isLogged){
                this._isLogged.raise();
            }
        }
        //endregion

        //region Events
        
        /**
         * Back field for event
         */
        private _isLogged: LatteEvent;
        
        /**
         * Gets an event raised when the user is logged
         *
         * @returns {LatteEvent}
         */
        get isLogged(): LatteEvent{
            if(!this._isLogged){
                this._isLogged = new LatteEvent(this);
            }
            return this._isLogged;
        }
        //endregion

        //region Properties
        //endregion

        //region components
        /**
         * Field for cardLogin property
         */
        private _cardLogin: MCard;

        /**
         * Gets card login
         *
         * @returns {MCard}
         */
        get cardLogin(): MCard {
            if (!this._cardLogin) {
                this._cardLogin = new MCard();
                let card = this._cardLogin;

                let imageAvatar = <Element<HTMLImageElement>> new MElement('img');
                imageAvatar.element.src = strings.imageGoplekWbSm;

                let avatar = new MCardAvatar();
                avatar.imageAvatar.add(imageAvatar);
                avatar.title = strings.login;
                avatar.subtitle = strings.appName;

                let imageMedia = <Element<HTMLImageElement>> new MElement('img');
                imageMedia.element.src = strings.imageGoplekPlane;

                let media = new MCardMedia();
                media.imageMedia.add(imageMedia);

                let supporting = new MCardSupporting();
                supporting.add(this.inputUser);
                supporting.add(this.inputPassword);

                let actions = new MCardActions();
                actions.itemsCenter.add(this.buttonLogin);

                card.add(this.progress);
                card.add(avatar);
                card.add(media);
                card.add(supporting);
                card.add(actions);
            }
            return this._cardLogin;
        }

        /**
         * Field for buttonLogin property
         */
        private _buttonLogin: MButton;

        /**
         * Gets button login
         *
         * @returns {MButton}
         */
        get buttonLogin(): MButton {
            if (!this._buttonLogin) {
                this._buttonLogin = new MButton(strings.login,null,'white', 'accent');
                this._buttonLogin.click.add(() => this.login(this.inputUser.value, this.inputPassword.value));
            }
            return this._buttonLogin;
        }

        /**
         * Field for inputUser property
         */
        private _inputUser: MInput;

        /**
         * Gets input user
         *
         * @returns {MInput}
         */
        get inputUser(): MInput {
            if (!this._inputUser) {
                this._inputUser = new MInput('username');

                let inputUser = this._inputUser;
                inputUser.label = strings.user;
                // inputUser.placeholder = strings.user;
                inputUser.type = 'text';
                inputUser.required = true;
                inputUser.icon = 'face';
                inputUser.iconAction = 'clear';
                inputUser.clickAction.add(() => {
                   inputUser.value = '';
                });

                inputUser.helper = 'Usuario de Goplek';
                inputUser.regexp = /([A-Za-z0-9]){4,}/;
                inputUser.errorMessage = 'Solo se aceptan letras';
            }
            return this._inputUser;
        }

        /**
         * Field for inputPassword property
         */
        private _inputPassword: MInput;

        /**
         * Gets input password
         *
         * @returns {MInput}
         */
        get inputPassword(): MInput {
            if (!this._inputPassword) {
                this._inputPassword = new MInput('password');

                let inputPassword = this._inputPassword;
                inputPassword.label = strings.password;
                // inputPassword.placeholder = strings.password;
                inputPassword.type = 'password';
                inputPassword.required = true;
                inputPassword.icon = 'lock';
                inputPassword.showPassword = true;
                inputPassword.helper = 'Contraseña proporcionada en Goplek';
                inputPassword.regexp = /([A-Za-z0-9]){4,}/;
                inputPassword.errorMessage = 'Ingrese Minimo 4 caracteres';
            }
            return this._inputPassword;
        }

        /**
         * Field for progress property
         */
        private _progress: MProgressIndeterminate;

        /**
         * Gets progress indeterminate
         *
         * @returns {MProgressIndeterminate}
         */
        get progress(): MProgressIndeterminate {
            if (!this._progress) {
                this._progress = new MProgressIndeterminate();
            }
            return this._progress;
        }

        /**
         * Field for toast property
         */
        private _toast: MToast;

        /**
         * Gets toast
         *
         * @returns {MToast}
         */
        get toast(): MToast {
            if (!this._toast) {
                this._toast = new MToast();
                let toast = this._toast;
                toast.action = "Aceptar";
                toast.color = "green";
                toast.click.add(() => {
                   alert("Reintentar");
                });
            }
            return this._toast;
        }
        //endregion
    }

}