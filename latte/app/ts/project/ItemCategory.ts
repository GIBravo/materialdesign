module latte {

    /**
     *
     */
    export class ItemCategory extends ItemCategoryBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        //endregion

        //region Methods

        /**
         * Raises the <c>category</c> event
         */
        onCategoryChanged(){
            if(this._categoryChanged){
                this._categoryChanged.raise();
            }

            if(this.category != null)
                this.name.text = this.category.name;
        }
        //endregion

        //region Events
        /**
         * Back field for event
         */
        private _categoryChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the category property changes
         *
         * @returns {LatteEvent}
         */
        get categoryChanged(): LatteEvent{
            if(!this._categoryChanged){
                this._categoryChanged = new LatteEvent(this);
            }
            return this._categoryChanged;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _category: Category = null;

        /**
         * Gets or sets category
         *
         * @returns {Category}
         */
        get category(): Category{
            return this._category;
        }

        /**
         * Gets or sets category
         *
         * @param {Category} value
         */
        set category(value: Category){

            // Check if value changed
            let changed: boolean = value !== this._category;

            // Set value
            this._category = value;

            // Trigger changed event
            if(changed){
                this.onCategoryChanged();
            }
        }
        //endregion

    }

}