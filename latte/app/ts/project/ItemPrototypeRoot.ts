module latte {

    /**
     *
     */
    export class ItemPrototypeRoot extends ItemPrototypeRootBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
        }

        //region Private Methods
        icon_click(){
            if(!this.itemDesktop.hasClass('hide')){
                this.itemDesktop.addClass('hide');
                this.itemMobile.addClass('show');
            }else{
                this.itemDesktop.removeClass('hide');
                this.itemMobile.removeClass('show');
            }
        }
        //endregion

        //region Methods
        //endregion

        //region Events
        //endregion

        //region Properties
        //endregion

    }

}