module latte {

    import ItemPrototypeRootBase = latte.ItemPrototypeRootBase;

    /**
     *
     */
    export class ViewProject extends ViewProjectBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.onLoad();
        }

        //region Private Methods
        private hideElement(element: Element<HTMLElement>, target: EventTarget, action?: Element<HTMLElement>){

            if(action && target == action.raw)
                element.removeClass('hide');
            else if(!element.hasClass('hide'))
                element.addClass('hide');
        }

        private onLoad(){
            this.MNavbar.add(this.navbar);
            this.MButton.add(this.buttonNewPrototype);
            this.MStateDevice.add(this.stateDevice);
            this.MInput.add(this.inputCategory);
            this.MList.add(this.listCategory);

            this.filePrototype.addEventListener('change',ev => {
                Prototype.create({
                    name: strings.newPrototype,
                    idcategory: this.categorySelected.idcategory
                }).send(prototype => {
                    this.uploadImage(ev.target.files[0], prototype.guid);
                    this.appendPrototype(prototype)
                });
            });
            this.loadCategories({trash: false});
        }
        //endregion

        //region Methods
        appendCategory(category: Category){
            let item = new MListItem(category.name);
            item.avatar = 'view_module';
            item.obj = category;
            item.click.add(() => {
                this.categorySelected = category;
            });
            this.listCategory.add(item);
        }

        appendPrototype(prototype: Prototype){

            let itemPrototype1 = new ItemPrototype();
            itemPrototype1.prototype = prototype;
            itemPrototype1.click.add(() => { this.prototypeSelected = prototype; });
            itemPrototype1.prototypeChanged.add(() => {

            });

            let itemPrototype2 = new ItemPrototype();
            itemPrototype2.prototype = prototype;
            itemPrototype2.click.add(() => { this.prototypeSelected = prototype; });
            itemPrototype2.prototypeChanged.add(() => {

            });

            let itemRoot = new ItemPrototypeRoot();

            itemRoot.itemDesktop.add(itemPrototype1);
            itemRoot.itemMobile.add(itemPrototype2);

            this.itemsPrototype.add(itemRoot);
        }

        loadCategories(options){
            this.listCategory.clear();
            Category.search(options).send(categories => {
                if(categories.length > 0){
                    categories.forEach(category => this.appendCategory(category));
                }
            });
        }

        loadPrototypes(category: Category){
            this.itemsPrototype.clear();
            Prototype.search({idcategory: category.idcategory}).send(prototypes => {
                if(prototypes.length > 0)
                    prototypes.forEach(prototype => this.appendPrototype(prototype));
            });
        }

        newCategory(options: object){
            Category.create(options).send(category => this.appendCategory(category));
        }

        newPrototype(options: object){
            Prototype.create(options).send(prototype => this.appendPrototype(prototype));
        }

        selectCategory(category: Category){
            let items = this.listCategory.getCollection();

            for(let i=0; i < items.length; i++){
                if( items[i].obj.idcategory == category.idcategory )
                    items[i].addClass('selected');
                else
                    items[i].removeClass('selected');
            }
        }

        uploadImage(file, guid){
            let formData = new FormData();
            formData.append('file', file);
            formData.append('guid', guid);

            let httpRequest = new XMLHttpRequest();
            //httpRequest.onreadystatechange = () => {
            //    if (httpRequest.readyState == 4) ;

            //};

            httpRequest.open('post', 'uploadImage.php', true);
            httpRequest.send(formData);
        }

        /**
         * Raises the <c>categorySelected</c> event
         */
        onCategorySelectedChanged(){
            if(this._categorySelectedChanged){
                this._categorySelectedChanged.raise();
            }

            this.selectCategory(this.categorySelected);
            this.loadPrototypes(this.categorySelected);
        }

        /**
         * Raises the <c>project</c> event
         */
        onProjectChanged(){
            if(this._projectChanged){
                this._projectChanged.raise();
            }

            if(this.project != null ){
            }
        }

        /**
         * Raises the <c>prototypeSelected</c> event
         */
        onPrototypeSelectedChanged(){
            if(this._prototypeSelectedChanged){
                this._prototypeSelectedChanged.raise();
            }
        }
        //endregion

        //region Events
        /**
         * Back field for event
         */
        private _projectChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the project property changes
         *
         * @returns {LatteEvent}
         */
        get projectChanged(): LatteEvent{
            if(!this._projectChanged){
                this._projectChanged = new LatteEvent(this);
            }
            return this._projectChanged;
        }

        /**
         * Back field for event
         */
        private _prototypeSelectedChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the prototypeSelected property changes
         *
         * @returns {LatteEvent}
         */
        get prototypeSelectedChanged(): LatteEvent{
            if(!this._prototypeSelectedChanged){
                this._prototypeSelectedChanged = new LatteEvent(this);
            }
            return this._prototypeSelectedChanged;
        }

        /**
         * Back field for event
         */
        private _categorySelectedChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the categorySelected property changes
         *
         * @returns {LatteEvent}
         */
        get categorySelectedChanged(): LatteEvent{
            if(!this._categorySelectedChanged){
                this._categorySelectedChanged = new LatteEvent(this);
            }
            return this._categorySelectedChanged;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _project: Project = null;

        /**
         * Gets or sets project
         *
         * @returns {Project}
         */
        get project(): Project{
            return this._project;
        }

        /**
         * Gets or sets project
         *
         * @param {Project} value
         */
        set project(value: Project){

            // Check if value changed
            let changed: boolean = value !== this._project;

            // Set value
            this._project = value;

            // Trigger changed event
            if(changed){
                this.onProjectChanged();
            }
        }

        /**
         * Property field
         */
        private _prototypeSelected: Prototype = null;

        /**
         * Gets or sets prototype selected
         *
         * @returns {Prototype}
         */
        get prototypeSelected(): Prototype{
            return this._prototypeSelected;
        }

        /**
         * Gets or sets prototype selected
         *
         * @param {Prototype} value
         */
        set prototypeSelected(value: Prototype){

            // Check if value changed
            let changed: boolean = value !== this._prototypeSelected;

            // Set value
            this._prototypeSelected = value;

            // Trigger changed event
            if(changed){
                this.onPrototypeSelectedChanged();
            }
        }

        /**
         * Property field
         */
        private _categorySelected: Category = null;

        /**
         * Gets or sets category selected
         *
         * @returns {Category}
         */
        get categorySelected(): Category{
            return this._categorySelected;
        }

        /**
         * Gets or sets category selected
         *
         * @param {Category} value
         */
        set categorySelected(value: Category){

            // Check if value changed
            let changed: boolean = value !== this._categorySelected;

            // Set value
            this._categorySelected = value;

            // Trigger changed event
            if(changed){
                this.onCategorySelectedChanged();
            }
        }
        //endregion

        //region Components
        /**
         * Field for buttonNewPrototype property
         */
        private _buttonNewPrototype: MButton;

        /**
         * Gets button new prototype
         *
         * @returns {MButton}
         */
        get buttonNewPrototype(): MButton {
            if (!this._buttonNewPrototype) {
                this._buttonNewPrototype = new MButton(strings.newPrototype,'image','accent');
                let button = this._buttonNewPrototype;

                let label = <Element <HTMLLabelElement>> new MElement('label');
                label.element.htmlFor = 'fileprototype';
                label.style.width = '100%';
                label.style.height = '100%';
                label.style.zIndex = '1';
                label.style.position = 'absolute';
                button.add(label);
            }
            return this._buttonNewPrototype;
        }

        /**
         * Field for inputCategory property
         */
        private _inputCategory: MInput;

        /**
         * Gets input category
         *
         * @returns {MInput}
         */
        get inputCategory(): MInput {
            if (!this._inputCategory) {
                this._inputCategory = new MInput('category');
                let input = this._inputCategory;
                input.placeholder = strings.newCategory;
                input.regexp = /([A-Za-z0-9]){4,}/;
                input.errorMessage = strings.errorCharacters;

                input.enter.add(() => {
                    this.newCategory({name: input.value});
                    input.value = '';
                });
            }
            return this._inputCategory;
        }

        /**
         * Field for navbar property
         */
        private _navbar: MNavbar;

        /**
         * Gets navbar
         *
         * @returns {MNavbar}
         */
        get navbar(): MNavbar {
            if (!this._navbar) {
                this._navbar = new MNavbar();
                let navbar = this._navbar;

                navbar.itemsLeft.add(this.buttonCategory);
            }
            return this._navbar;
        }

        /**
         * Field for stateDevice property
         */
        private _stateDevice: MState;

        /**
         * Gets state device
         *
         * @returns {MState}
         */
        get stateDevice(): MState {
            if (!this._stateDevice) {
                this._stateDevice = new MState();
                let state = this._stateDevice;
                state.icons = ['laptop_mac','devices','phone_iphone'];
                state.state = 1;
                state.stateChanged.add(() => {

                    let options = {
                        trash: false
                    };

                    if( this.stateDevice.state != 1 )
                        options['type'] = (state.state == 0)? 0:1;

                    this.loadCategories(options);
                });
            }
            return this._stateDevice;
        }

        /**
         * Field for labelFilePrototype property
         */
        private _labelFilePrototype: MElement;

        /**
         * Field for listCategory property
         */
        private _listCategory: MList;

        /**
         * Gets category list
         *
         * @returns {MList}
         */
        get listCategory(): MList {
            if (!this._listCategory) {
                this._listCategory = new MList();
                this.listHeading.text = "Categorias";
            }
            return this._listCategory;
        }

        /**
         * Field for buttonCategory property
         */
        private _buttonCategory: MButtonIcon;

        /**
         * Gets button category (to show categories)
         *
         * @returns {MButtonIcon}
         */
        get buttonCategory(): MButtonIcon {
            if (!this._buttonCategory) {
                this._buttonCategory = new MButtonIcon('menu', 'white');
                let button = this._buttonCategory;

                button.click.add(() => {
                    if(this.barLeft.hasClass('hide'))
                        this.barLeft.removeClass('hide');
                    else
                        this.barLeft.addClass('hide');
                });
            }
            return this._buttonCategory;
        }
        //endregion

    }

}