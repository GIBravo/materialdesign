module latte {

    /**
     *
     */
    export class ItemPrototype extends ItemPrototypeBase {

        //region Static
        //endregion

        //region Fields
        //endregion

        /**
         *
         */
        constructor() {
            super();
            this.onLoad();
        }

        //region Private Methods
        private changeName(name: string){
            if (/([A-Za-z0-9]){4,}/.exec(name)) {
                this.prototype.name = name;
                this.prototype.save();
            } else
                this.name.text = this.prototype.name;
        }

        private setStatus(){
            if(this.prototype.archived){
                this.status.style.background = 'var(--red)';
            }
            else{
                this.status.style.background = 'var(--green)';
            }
        }

        private archive(){
            this.prototype.archived = !this.prototype.archived;
            this.prototype.save(() => {
               this.setStatus();
            });
        }

        private delete(){
            this.prototype.trash = true;
            this.prototype.save(() => {
                this.prototype = null;
            });
        }

        private share(){

        }

        private onLoad(){
            this.MMenu.add(this.menu);
            LocalEditor.onClick(this.name.raw,() => this.changeName(this.name.text));
        }
        //endregion

        //region Methods

        status_click(){
            this.archive();
        }

        /**
         * Raises the <c>click</c> event
         */
        onClick(){
            if(this._click){
                this._click.raise();
            }
        }

        /**
         * Raises the <c>prototype</c> event
         */
        onPrototypeChanged(){
            if(this._prototypeChanged){
                this._prototypeChanged.raise();
            }

            if(this.prototype != null){
                this.image = this.prototype.guid;
                this.name.text = this.prototype.name;
                this.type.text = (this.prototype.type == 1)? 'laptop_mac' : 'phone_iphone';
                this.setStatus();
            }
        }
        //endregion

        //region Events

        /**
         * Back field for event
         */
        private _click: LatteEvent;

        /**
         * Gets an event raised when click
         *
         * @returns {LatteEvent}
         */
        get click(): LatteEvent{
            if(!this._click){
                this._click = new LatteEvent(this);
            }
            return this._click;
        }

        /**
         * Back field for event
         */
        private _prototypeChanged: LatteEvent;

        /**
         * Gets an event raised when the value of the prototype property changes
         *
         * @returns {LatteEvent}
         */
        get prototypeChanged(): LatteEvent{
            if(!this._prototypeChanged){
                this._prototypeChanged = new LatteEvent(this);
            }
            return this._prototypeChanged;
        }
        //endregion

        //region Properties
        /**
         * Property field
         */
        private _prototype: Prototype = null;

        /**
         * Gets or sets prototype
         *
         * @returns {Prototype}
         */
        get prototype(): Prototype{
            return this._prototype;
        }

        /**
         * Gets or sets prototype
         *
         * @param {Prototype} value
         */
        set prototype(value: Prototype){

            // Check if value changed
            let changed: boolean = value !== this._prototype;

            // Set value
            this._prototype = value;

            // Trigger changed event
            if(changed){
                this.onPrototypeChanged();
            }
        }
        //endregion

        //region Components
        /**
         * Field for menu property
         */
        private _menu: MMenu;

        /**
         * Gets menu
         *
         * @returns {MMenu}
         */
        get menu(): MMenu {
            if (!this._menu) {
                this._menu = new MMenu();
                let menu = this._menu;

                let menuDelete = new MMenuItem();
                menuDelete.text = strings.delete;
                menuDelete.click.add(() => this.delete());

                let menuStatus = new MMenuItem();
                menuStatus.text = strings.toArchive;
                menuStatus.click.add(() => this.archive());

                let menuShare = new MMenuItem();
                menuShare.text = strings.share;
                menuShare.click.add(() => this.share());

                menu.items.add(menuShare);
                menu.items.add(menuStatus);
                menu.items.add(menuDelete);
            }
            return this._menu;
        }

        /**
         * Property field
         */
        private _image: string = null;

        /**
         * Gets or sets image
         *
         * @returns {string}
         */
        get image(): string {
            return this._image;
        }

        /**
         * Gets or sets image
         *
         * @param {string} value
         */
        set image(value: string) {
            this._image = value;

            let imageMedia = <Element<HTMLImageElement>> new MElement('img');
            imageMedia.element.src = strings.pathImages + value + ".png";
            this.boxImage.add(imageMedia);
        }
        //endregion
    }

}