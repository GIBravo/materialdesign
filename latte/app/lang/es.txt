imageBackground /images/dot.png
imageGoplek /images/goplek.png
imageGoplekBlack /images/goplek-black.png
imageGoplekOriginal /images/goplek-original.png
imageGoplekPlane /images/goplek-plane.png
imageGoplekWb /images/goplek-wb.png
imageGoplekWbSm /images/goplek-wb-sm.png
imageGoplekWhite /images/goplek-white.png

errorCharacters Solo se aceptan letras
errorUsername Verifica tu nombre de usuario
errorPassword Verifica tu contraseña
errorSession Usuario y/o Contraseña incorrecto

newCategory Nueva Categoria
newProject Nuevo Proyecto
newPrototype Nuevo Prototipo

pathImages /prototypes/

delete Borrar
archive Archivo
archived Archivado
toArchive Archivar
share Compartir

appName Visor de Prototipos
login iniciar sesión
logout cerrar sesión
password Contraseña
user Usuario